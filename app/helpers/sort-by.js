import Ember from 'ember';

// {{my-helper 1 2 3 bar="baz"}}
// [1,2,3], {bar: "baz"}
export function sortBy([array, propertyName], {desc = false}) {
  let sortedArray = array.sortBy(propertyName);
  if (desc) { sortedArray = sortedArray.reverse(); }
  return sortedArray;
}

export default Ember.Helper.helper(sortBy);
